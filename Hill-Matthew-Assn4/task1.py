# Matthew Hill
# CS 1400, 002
# Assignment 4 task 1
# Uses turtle to draw an amazing snowman, who could potentially become my best friend :)
import turtle

# start
# turn the following comment into code to hide the turtle throughout the program.
# turtle.hideturtle()
turtle.penup()

# Makes head
turtle.goto(25, 125)  # head center at (0, 125)
turtle.left(90)
turtle.color("black")
turtle.begin_fill()
turtle.fillcolor("white")
turtle.pendown()
turtle.circle(25)
turtle.penup()
turtle.end_fill()

# Makes middle
turtle.goto(50, 50)  # middle center at (0, 50)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("white")
turtle.circle(50)
turtle.penup()
turtle.end_fill()

# makes bottom
turtle.goto(75, -75)  # bottom center at (0, -75)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("white")
turtle.circle(75)
turtle.penup()
turtle.end_fill()

# makes hat
turtle.goto(15, 145)
turtle.setheading(90)
turtle.color("black")
turtle.begin_fill()
turtle.fillcolor("black")
turtle.pendown()
turtle.forward(30)
turtle.left(90)
turtle.forward(30)
turtle.left(90)
turtle.forward(30)
turtle.left(90)
turtle.forward(30)
turtle.end_fill()
turtle.penup()
# makes hat brim
turtle.goto(25, 145)
turtle.setheading(180)
turtle.pendown()
turtle.width(4)
turtle.forward(50)
turtle.penup()
turtle.width(1)
turtle.setheading(90)
turtle.color("black")

# makes eyes
turtle.goto(15, 130)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("black")
turtle.circle(3)
turtle.penup()
turtle.end_fill()

turtle.goto(-15, 130)
turtle.right(180)
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("black")
turtle.circle(3)
turtle.penup()
turtle.end_fill()

# makes mouth
turtle.goto(-13, 115)
turtle.setheading(315)
turtle.pendown()
turtle.width(4)
turtle.circle(20, 90)
turtle.width(1)
turtle.penup()
turtle.setheading(90)

# makes button 1
turtle.goto(5, 75)
turtle.color("blue")
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("blue")
turtle.circle(5)
turtle.penup()
turtle.end_fill()

# makes button 2
turtle.goto(5, 50)
turtle.color("green")
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("green")
turtle.circle(5)
turtle.penup()
turtle.end_fill()

# makes button 3
turtle.goto(5, 25)
turtle.color("red")
turtle.pendown()
turtle.begin_fill()
turtle.fillcolor("red")
turtle.circle(5)
turtle.penup()
turtle.end_fill()

turtle.color("brown")

# makes right arm
turtle.goto(50, 50)
turtle.setheading(315)
turtle.pendown()
turtle.width(4)
turtle.forward(70) # end of arm is at (100, 0)
turtle.penup()
# makes right fingers
turtle.setheading(280)
turtle.pendown()
turtle.forward(20)
turtle.penup()

turtle.goto(100, 0)
turtle.setheading(315)
turtle.pendown()
turtle.forward(20)
turtle.penup()

turtle.goto(100, 0)
turtle.setheading(350)
turtle.pendown()
turtle.forward(20)
turtle.penup()

# makes left arm
turtle.goto(-50, 50)
turtle.setheading(225)
turtle.pendown()
turtle.width(4)
turtle.forward(70) # end of arm is at (-100, 0)
turtle.penup()

# makes left fingers
turtle.setheading(260)
turtle.pendown()
turtle.forward(20)
turtle.penup()

turtle.goto(-100, 0)
turtle.setheading(225)
turtle.pendown()
turtle.forward(20)
turtle.penup()

turtle.goto(-100, 0)
turtle.setheading(190)
turtle.pendown()
turtle.forward(20)
turtle.penup()

# end
turtle.hideturtle()
turtle.goto(0, 0)
turtle.done()
