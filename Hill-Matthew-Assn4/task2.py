# Matthew Hill
# CS 1400, 002
# Assignment 4 task 2
# User inputs the dimensions of a cuboid, the program outputs the volume and SA of the cuboid.

print("This program will compute the surface area and volume of a rectangular prism with " \
      "dimensions (in feet) that you give.\n")

length = eval(input("What is the length?: "))
width = eval(input("What is the width?: "))
height = eval(input("What is the height?: "))

volume = length * width * height
surfaceArea = (2 * (length * width)) + (2 * (length * height)) + (2 * (width * height))

print("The rectangular prism given is", length, "X", width, "X", height, ".")
print("The volume is", volume, "cubic feet.")
print("The surface area is", surfaceArea, "square feet.")