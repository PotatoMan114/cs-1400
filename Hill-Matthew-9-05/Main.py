# this is a single line comment
'''
    This is a
    Multi-line
    comment
'''


# Name
# Course-Section
# Assignment #

print("one")
# print("two")
print("three")

print("one")
print('one')

print("5 + 3")
print(5 + 3)
print("5 + 3 = ", 5 + 3)
