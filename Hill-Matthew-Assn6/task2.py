# Matthew Hill
# CS 1400 002
# Assignment 6 Task 2
# This program takes employee data and outputs it with special formatting.

# All user inputs:
name = input("Enter the employee's name: ").strip()
hoursWorked = eval(input("Enter the number of hours worked: "))
payRate = eval(input("Enter the employee's pay: "))
fedRate = eval(input("Enter the federal withholding rate (ex. 0.14): "))
stateRate = eval(input("Enter the state withholding rate: (ex. 0.14): "))

# Calculations
grossPay = payRate * hoursWorked
fedHold = fedRate * grossPay
stateHold = stateRate * grossPay
totalDeductions = fedHold + stateHold
netPay = grossPay - totalDeductions

# Formatting lines to prepare for output.
line1 = format(name.upper() + "'S PAY INFORMATION", "^40.40s") + "\n"
line2 = "\n"
line3 = format("Pay", "^40.40s") + "\n"
line4 = str(format("Hours Worked:", ">30.30s") + format(hoursWorked, ">10.2f")) + "\n"
line5 = str(format("Pay Rate:", ">30.30s") + format("$", ">2.2s") + format(payRate, ">8.2f")) + "\n"
line6 = str(format("Gross Pay:", ">30.30s") + format("$", ">2.2s") + format(grossPay, ">8.2f")) + "\n"
line7 = "\n"
line8 = format("Deduction", "^40.40s") + "\n"
line9 = str(format("Federal Withholding (" + format(fedRate * 100, "4.1f") + "%):", ">30.30s") + format("$", ">2.2s") +
            format(fedHold, ">8.2f")) + "\n"
line10 = str(format("State Withholding (" + format(stateRate * 100, "4.1f") + "%):", ">30.30s") + format("$", ">2.2s") +
             format(stateHold, ">8.2f")) + "\n"
line11 = str(format("Total Deduction:", ">30.30s") + format("$", ">2.2s") + format(totalDeductions, ">8.2f")) + "\n"
line12 = "\n"
line13 = str(format("Net Pay:", ">30.30s") + format("$", ">2.2s") + format(netPay, ">8.2f")) + "\n"

# print
print("\n" + line1 + line2 + line3 + line4 + line5 + line6 + line7 + line8 + line9 + line10 + line11 + line12 + line13)
