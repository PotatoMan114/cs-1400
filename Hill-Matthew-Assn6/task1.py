# Matthew Hill
# CS 1400-002
# Assignment 6, task 1
# This program will output the are of a regular polygon
#    when given the number of sides and side length.

import math
from math import tan

print("This program will output the area of a regular polygon.\n")

numSides = eval(input("Enter the number of sides: "))
sideLength = eval(input("Enter the side length: "))

area = (numSides * math.pow(sideLength, 2)) / (4 * tan(math.pi/numSides))

print("The area is " + str(round(area, 5)))

