System Requirements:
    user inputs x,y position for the bottom left corner of the board
    user inputs width and height of the board
    system uses turtle to draw one chessboard based on user's input.
    boards to not overlap

System Analysis:
    individual rectangle height = total height / 8
    individual rectangle width = total height / 8

    right side of the board (x-value) = startX + width
    top of the board (y-value) = startY + height

System Design:
    must use a custom module
        import turtle
        module includes three functions:
            drawChessboard(startX, startY, width=250, height=250):
                default width and height of 250
                turtle goes to bottom left corner of board (startX, startY)
                Draws the boarder of the board.
                calls drawAllRectangles()
            drawAllRectangles():
                calls drawRectangle in a nested loop, changing the starting x, y with each loop. (outer is x, inner is y)
                    bottom left corner is a black box
                    draw every other row at a time.
                    two loops (two outer and two inner loops complete separate) because of this.
                only needs to draw black rectangles
                four rectangles in a row.
            drawRectangle():
                draws an individual rectangle based on a given x, y, height, and width.
                    draws a side then rotates 90 degrees
                    repeat this for all sides.
                see System Analysis for individual height and width calculations.
                turtle ends in the same position it starts.
        # only need to draw four black rectangles per column / row

    main is only function defined in assn9.py:
        prompt the user for x, y input
        prompt the user for height input
        prompt the user for width input
            Assume user inputs correctly

        call drawChessboard with input from user.

    call main

Testing:
    user inputs (-100, -100) as x, y
    user leaves width and height blank
    turtle draws a 250 by 250 8x8 board

    user inputs (-100, -100) as x, y
    user inputs 50 as width and 200 as height
    turtle draws a 50 by 200 8x8 board (each space is correctly sized)


