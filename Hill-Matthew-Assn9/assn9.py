# Matthew Hill
# CS 1400 002
# Assignment 9
# This is the file that will be run. It will draw a chessboard with input from the user.

import turtle

from chessboard import drawChessboard


def main():

    # input code
    startX, startY = eval(input("Enter the x,y for the bottom left corner of the board: "))
    width = input("Enter the width of the board: ")
    height = input("Enter the height of the board: ")
    turtle.speed(50)
    if width == "" and height == "":
        drawChessboard(startX, startY)
    elif height == "":
        drawChessboard(startX, startY, width=eval(width))
    elif width == "":
        drawChessboard(startX, startY, height=eval(height))
    else:
            drawChessboard(startX, startY, eval(width), eval(height))

    turtle.hideturtle()
    turtle.done()


main()
