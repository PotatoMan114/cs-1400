# Matthew Hill
# CS 1400 002
# Assignment 9 Module chessboard
# This is the module with the functions that draw the chessboard.

import turtle


# Draw board outline, then call drawAllRectangles()
def drawChessboard(startX, startY, width=250, height=250):
    # turtle goes to x, y
    turtle.penup()
    turtle.goto(startX, startY)

    # draw the border
    turtle.pendown()
    for i in range(0, 2):
        turtle.forward(width)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)
    turtle.penup()
    drawAllRectangles(startX, startY, width, height)


# draws all the black rectangles by calling drawRectangle()
def drawAllRectangles(startX, startY, width, height):
    oneWidth = width / 8  # the width of one rectangle
    oneHeight = height / 8  # the height of one rectangle

    for x in range(startX, int(startX + width - oneWidth + 1), int(oneWidth * 2)):
        for y in range(startY, int(startY + height - oneHeight + 1), int(oneHeight * 2)):
            turtle.penup()
            turtle.setheading(0)
            turtle.goto(x, y)
            drawRectangle(oneWidth, oneHeight)

    for x in range(int(startX + oneWidth), startX + width + 1, int(oneWidth * 2)):
        for y in range(int(startY + oneHeight), startY + height + 1, int(oneHeight * 2)):
            turtle.penup()
            turtle.setheading(0)
            turtle.goto(x, y)
            drawRectangle(oneWidth, oneHeight)

# draws one rectangle
def drawRectangle(width, height):
    turtle.pendown()
    turtle.fillcolor("black")
    turtle.begin_fill()
    for i in range(0, 2):
        turtle.forward(width)
        turtle.left(90)
        turtle.forward(height)
        turtle.left(90)
    turtle.penup()
    turtle.end_fill()

