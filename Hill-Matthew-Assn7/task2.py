# Matthew Hill
# CS 1400 002
# Assn 7 Task 2
# States if two circles overlap or are inside of each other based on user input

from math import sqrt

x1, y1 = eval(input("Enter the center position of circle 1 (x, y): "))
radius1 = eval(input("Enter the radius of circle 1: "))
x2, y2 = eval(input("Enter the center position of circle 2 (x, y): "))
radius2 = eval(input("Enter the radius of circle 2: "))
print()
circle1Larger = radius1 > radius2

distance = sqrt((abs(x1 - x2) ** 2) + (abs(y1 - y2) ** 2))  # Pythagorean Theorem

circle1Inside = circle2Inside = False  # Creates these variables so that both are made for later if and elif statements.

if radius1 > radius2:
    circle2Inside = distance <= abs(radius2 - radius1)
elif radius2 > radius1:
    circle1Inside = distance <= abs(radius1 - radius1)

if circle2Inside:
    print("Circle 2 is inside of circle 1.")
elif circle1Inside:
    print("Circle 1 is inside of circle 1.")
else:
    overlapping = distance <= radius1 + radius2
    if overlapping and radius1 > radius2:
        print("Circle 2 overlaps circle 1.")
    elif overlapping and radius2 > radius1:
        print("Circle 1 overlaps circle 2.")
    else:
        print("The circles do not overlap at all.")

