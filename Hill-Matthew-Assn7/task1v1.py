# Matthew Hill
# CS 1400 002
# Assn 7, Task 1, Version 1
# Create Rock, Paper, Scissors without using logical operators.

import random
import time
import sys

print("Welcome to Rock, Paper, Scissors!\nYou will be playing against the computer!\n")

user = eval(input("Please select scissors (0), rock (1), or paper (2): "))
random.seed(time.time())
com = random.randint(0, 2)

# tells user what they inputted
if user == 0:
    print("You chose scissors.")
elif user == 1:
    print("You chose rock.")
elif user == 2:
    print("You chose paper.")
else:  # invalid input
    print("Invalid input.")
    sys.exit(1)

# tells user what the computer chose
if com == 0:
    print("The computer chose scissors.")
elif com == 1:
    print("The computer chose rock.")
elif com == 2:
    print("The computer chose paper.")
else:  # invalid computer pick
    print("Invalid computer range.")
    sys.exit(2)

# tests who won
if user == com:
    print("It's a draw!")
elif user == 0:
    if com == 1:
        print("Rock beats scissors! The computer wins!")
    elif com == 2:
        print("Scissors beats paper! The user wins!")
elif user == 1:
    if com == 0:
        print("Rock beats scissors! The user wins!")
    elif com == 2:
        print("Paper beats rock! The computer wins!")
elif user == 2:
    if com == 0:
        print("Scissors beats paper! The computer wins!")
    elif com == 1:
        print("Paper beats rock! The user wins!")
