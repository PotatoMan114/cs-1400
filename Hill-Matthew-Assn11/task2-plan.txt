System requirements
    Program simulates a bank account.

    User inputs their account's ID, balance, and annual interest rate.

    User selects one of the following:
        (1): Display ID
            Displays the ID of the account
        (2): Display Balance
            Displays the account's balance
        (3): Display Annual Interest Rate
            Displays the account's annual interest rate
        (4): Display Monthly Interest Rate
            Displays the account's monthly interest rate
        (5): Display Monthly Interest
            Displays the account's monthly interest
        (6): Withdraw Money
            takes money out of the account
        (7): Deposit Money
            puts money in the account
        (8): Exit
            Exits the program.

    if the user inputs something incorrectly, they are prompted to try again.

System Analysis
    monthlyInterest: balance * monthlyInterestRate
    monthlyInterestRate: annualInterestRate / 12
    annualInterestRate (decimal) = annualInterestRate (percent) / 100


System Design
    UML:
        Account
        ___________________________________________________________________________
        id: int
        balance: float
        annualInterestRate: float
        ___________________________________________________________________________
        Television(id=0: int, balance=100: float, annualInterestRate=0: float)

        getId(): int
        getBalance(): float
        getAnnualInterestRate(): float
        setId(id: int): None
        setBalance(balance: float): None
        setAnnualInterestRate(annualInterestRate: float): None
        getMonthlyInterestRate(): float
        getMonthlyInterest(): float
        withdraw(amount=0: float): None
        deposit(amount=0: float): None
        ___________________________________________________________________________


    main():

        prompt user to input id, balance, and annual interest rate
            use the setters in the Account class.
            prompt the user to try again if input is outside of accepted range (see rubric).


        loop while exit == False:
            main menu:
                (1): Display ID
                    use getId() from Account

                (2): Display Balance
                    use getBalance from Account

                (3): Display Annual Interest Rate
                    use getAnnualInterestRate() from Account

                (4): Display Monthly Interest Rate
                    use getMonthlyInterestRate() from Account

                (5): Display Monthly Interest
                    use getMonthlyInterest() from Account

                (6): Withdraw Money
                    use withdraw() from Account
                    must be positive and less than or equal to the account's balance.

                (7): Deposit Money
                    use deposit() from Account
                    must be positive

                (8): Exit
                    exit the loop.

            if any other number is inputted, prompt them to try again.


Testing
    test 1 (all valid):
        Id: 5
        Balance: 100
        ann. int. rate: 5%

        menu displays correctly.

        chose 1:
            output: ID: 5
                correct

        chose 2:
            output: Balance: $100
                correct

        chose 3:
            output: Annual Interest Rate: 5%
                correct

        chose 4:
            Monthly Interest Rate: 0.4166666666666667%
                correct

        chose 5:
            output: Monthly Interest: 0.4166666666666667
                correct

        chose 6:
            output: Enter the amount you want to withdraw:
            entered 50
            output: New Balance: $50
                correct

        chose 7:
            output: Enter the amount you want to deposit:
            Entered 50
            output: New Balance: $100
                correct
        chose 8:
            output:
            Thanks for using the The Python Bank of Programmers!

            Process finished with exit code 0

                correct

        all are correct


    test 2 (all invalid input):
        Id: -5
            correct detection of invalid input
        Balance: -100
            correct detection of invalid input
        ann. int. rate: -5
            correct detection of invalid input
        ann. int. rate: 14
            correct detection of invalid input

        menu displays correctly.

        chose 6:
            output: Enter the amount you want to withdraw:
            entered -50
                correct detection of invalid input

        chose 7:
            output: Enter the amount you want to deposit:
            Entered -50
                correct detection of invalid input

        all are correct

