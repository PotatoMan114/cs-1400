# Matthew Hill
# CS 1400 002
# Assignment 11 Task 1
# The program draws a face! Use starter code and fill in the blanks.

import turtle

class Face:
    def __init__(self):
        self.__smile = True
        self.__happy = True
        self.__dark_eyes = True

    def draw_face(self):
        turtle.clear()
        self.__draw_head()
        self.__draw_eyes()
        self.__draw_mouth()

    def __draw_head(self):
        turtle.penup()
        turtle.setheading(0)
        turtle.goto(0,-100)
        turtle.fillcolor("yellow") if self.is_happy() else turtle.fillcolor("red")
        turtle.begin_fill()
        turtle.pendown()
        turtle.circle(100)
        turtle.penup()
        turtle.end_fill()

    def __draw_eyes(self):
        turtle.penup()
        turtle.setheading(0)
        turtle.goto(-25, 25)
        turtle.fillcolor("black") if self.is_dark_eyes() else turtle.fillcolor("blue")
        turtle.begin_fill()
        turtle.pendown()
        turtle.circle(10)
        turtle.penup()
        turtle.goto(25, 25)
        turtle.pendown()
        turtle.circle(10)
        turtle.penup()
        turtle.end_fill()

    def __draw_mouth(self):
        turtle.penup()
        turtle.width(5)
        if self.is_smile():
            turtle.goto(-50, -50)
            turtle.setheading(-45)
            turtle.pendown()
            turtle.circle(75, 90)
            turtle.penup()
        else:
            turtle.goto(50, -50)
            turtle.setheading(-(180+45))
            turtle.pendown()
            turtle.circle(75, 90)
            turtle.penup()
        turtle.width(1)

    def is_smile(self):
        return self.__smile

    def is_happy(self):
        return self.__happy

    def is_dark_eyes(self):
        return self.__dark_eyes

    def change_mouth(self):
        self.__smile = not self.__smile

        self.draw_face()

    def change_emotion(self):
        self.__happy = not self.__happy

        self.draw_face()

    def change_eyes(self):
        self.__dark_eyes = not self.__dark_eyes

        self.draw_face()

def main():
    face = Face()
    face.draw_face()

    done = False

    while not done:
        print("Change My Face")
        mouth = "frown" if face.is_smile() else "smile"
        emotion = "angry" if face.is_happy() else "happy"
        eyes = "blue" if face.is_dark_eyes() else "black"

        print("1) Make me", mouth)
        print("2) Make me", emotion)
        print("3) Make my eyes", eyes)
        print("0) Quit")

        menu = eval(input("Enter a selection: "))

        if menu == 1:
            face.change_mouth()
        elif menu == 2:
            face.change_emotion()
        elif menu == 3:
            face.change_eyes()
        else:
            break

    print("Thanks for Playing")

    turtle.hideturtle()
    turtle.done()


main()