# Matthew Hill
# CS 1400 002
# Assignment 11 Task 2
# File that user will run. Will simulate a bank account using an Account class from account.py

from account import Account

def main():
    account = Account()
    exit = False
    valid = False
    print("Welcome to The Python Bank of Programmers!")
    while not valid:
        id = int(eval(input("Enter your account's ID: ")))
        if id < 0:
            print("Please enter a positive number.")
        else:
            account.setId(id)
            valid = True

    valid = False
    while not valid:
        balance = eval(input("Enter your account's balance: "))
        if balance < 0:
            print("Please enter a positive number.")
        else:
            account.setBalance(balance)
            valid = True

    valid = False
    while not valid:
        annualInterestRate = eval(input("Enter your account's annual interest rate (%): "))
        if annualInterestRate < 0:
            print("Please enter a positive number.")
        elif annualInterestRate > 10:
            print("Too big, sorry. Try again.")
        else:
            account.setAnnualInterestRate(annualInterestRate)
            valid = True

    valid = False
    while not exit:
        option = 0
        print("What would you like to do?")
        print("1): Display ID")
        print("2): Display Balance")
        print("3): Display Annual Interest Rate")
        print("4): Display Monthly Interest Rate")
        print("5): Display Monthly Interest")
        print("6): Withdraw Money")
        print("7): Deposit Money")
        print("8): Exit")
        option = eval(input("Choose: "))

        # Display ID
        if option == 1:
            print("ID: " + str(account.getId()))

        # Display Balance
        elif option == 2:
            print("Balance: $" + str(account.getBalance()))

        # Display Annual Interest Rate
        elif option == 3:
            print("Annual Interest Rate: " + str(account.getAnnualInterestRate()) + "%")

        # Display Monthly Interest Rate
        elif option == 4:
            print("Monthly Interest Rate: " + str(account.getMonthlyInterestRate()) + "%")

        # Display Monthly Interest
        elif option == 5:
            print("Monthly Interest: " + str(account.getMonthlyInterest()))

        # Withdraw Money
        elif option == 6:
            valid = False
            while not valid:
                amount = eval(input("Enter the amount you want to withdraw: "))
                if amount < 0:
                    print("Please enter a positive number.")
                elif amount > account.getBalance():
                    print("You can't take out more than you have. Go get a loan for that.")
                else:
                    account.withdraw(amount)
                    valid = True
            valid = False
            print("New Balance: $" + str(account.getBalance()))
        # Deposit Money
        elif option == 7:
            valid = False
            while not valid:
                amount = eval(input("Enter the amount you want to deposit: "))
                if amount < 0:
                    print("Please enter a positive number.")
                else:
                    account.deposit(amount)
                    valid = True
            valid = False
            print("New Balance: $" + str(account.getBalance()))
        # Exit
        elif option == 8:
            exit = True
        else:
            print("Try again.")

    print("Thanks for using the The Python Bank of Programmers!")


main()
