# Matthew Hill
# CS 1400 002
# Assignment 15 Task 2
# BlackJack! kinda...

from card import Card
from deck import Deck
from time import sleep

def cardPoints(card):
    value = -1
    if (card.getCardValue() - 1) % 13 == 0:
        value = 11
    elif (card.getCardValue() - 1) % 13 >= 1 and (card.getCardValue() - 1) <= 9:
        value = card.getCardValue() % 13
    elif (card.getCardValue() - 1) % 13 >= 10:
        value = 10
    return value


def dealCards(playerCount, deck, playersOut):

    hands = [[] for i in range(playerCount + 1)]  # +1 for the dealer
    for i in range(2):
        for j in range(playerCount + 1):
            if j in playersOut:
                hands[j].append(None)
                hands[j].append(None)
            hands[j].append(deck.draw())
            if i == 2 - 1 and j == playerCount:
                print("Dealer's second card: " + str(hands[j][i]))
    return hands


def isBust(hand):
    totalPoints = 0
    for i in range(len(hand)):
        totalPoints += cardPoints(hand[i])
    if totalPoints > 21:
        return True
    else:
        return False


def sort(list1):
    for i in range(len(list1) - 1):
        curMinNumber = i

        for j in range(i + 1, len(list1)):
            if list1[curMinNumber] < list1[j]:
                curMinNumber = j

        if curMinNumber != i:
            list1[i], list1[curMinNumber] = list1[curMinNumber], list1[i]


def main():
    deck = Deck()
    playersOut = []

    valid = False
    while not valid:
        playerCount = eval(input("How many players will play? (1-5)"))
        if playerCount >= 1 and playerCount <= 5:
            valid = True
            playerAccounts = [100] * playerCount
        else:
            valid = False
            playerCount = 0
            print("1-5, please.")

    anotherRound = True
    while anotherRound:

        deck.shuffle()

        playerBets = [0] * playerCount
        for i in range(playerCount):
            if i in playersOut:  # player i is out
                playerBets[i] = -1
                continue
            betAmount = 0
            if playerAccounts[i] <= 5:
                print("Player " + str(i + 1) + " is betting " + str(playerAccounts[i]) + " $.")
                betAmount = playerAccounts[i]
            else:
                valid = False
                print("Player " + str(i + 1) + ", how much will you bet?")
                print("You have " + str(playerAccounts[i]) + "$")
                while not valid:
                    betAmountRaw = input()
                    if betAmountRaw.isdigit():
                        betAmount = eval(betAmountRaw)
                        if betAmount > playerAccounts[i]:
                            print("You can't bet more than you have. Try again.")
                        elif betAmount < 5:
                            print("Don't wimp out. Try again.")
                        else:
                            valid = True
                    else:
                        print("A number, please.")
            playerBets[i] = betAmount

        hands = dealCards(playerCount, deck, playersOut)
        # print(hands)

        for i in range(playerCount):
            if i in playersOut:  # if player is out.
                continue
            print("\n\n\n\n\n")
            print("    PLAYER " + str(i + 1) + "    ")
            input("Press enter to continue.")
            print("\n\n\n\n\n")
            print("    PLAYER " + str(i + 1) + "    ")

            handPointTotal = 0
            for j in range(len(hands[i])):
                handPointTotal += cardPoints(hands[i][j])
            done = False
            while not done:
                print("Your hand:")
                for j in range(len(hands[i])):
                    print(hands[i][j])
                print("Your hand's total value: " + str(handPointTotal))
                if isBust(hands[i]):
                    print("Bust!")
                    done = True
                    break
                print("What would you like to do?")
                print("1) Hit")
                print("2) Hold")
                valid = False
                while not valid:
                    choiceRaw = input("Choose: ")
                    if choiceRaw.isdigit():
                        choice = eval(choiceRaw)
                        if choice == 1 or choice == 2:
                            valid = True
                        else:
                            print("1 or 2!")
                    else:
                        print("Please enter a number.")

                # Hit
                if choice == 1:
                    hands[i].append(deck.draw())
                    handPointTotal += cardPoints(hands[i][len(hands[i]) - 1])

                # Hold
                elif choice == 2:
                    done = True
                    print("Your point total is " + str(handPointTotal))

        dealerIndex = playerCount
        print("All players are done. Dealer's turn:")
        sleep(1)

        done = False
        while not done:
            print("Dealer hand:")
            for i in range(len(hands[dealerIndex])):
                print(hands[len(hands) - 1][i])
            sleep(1)
            handPointTotal = 0
            for i in range(len(hands[dealerIndex])):
                handPointTotal += cardPoints(hands[dealerIndex][i])
            if handPointTotal < 17:
                hands[dealerIndex].append(deck.draw())
                print("Dealer draws another card.")
            elif handPointTotal >= 17 and handPointTotal <= 21:
                print("The dealer holds.")
                done = True
            else:
                print("The dealer busts.")
                done = True

        handPointTotals = [0 for i in range(playerCount + 1)]
        handPointTotals[dealerIndex] = handPointTotal
        print("The dealer's hand is worth " + str(handPointTotals[dealerIndex]))
        for i in range(playerCount):
            if i in playersOut:
                continue
            handPointTotal = 0
            for j in range(len(hands[i])):
                handPointTotal += cardPoints(hands[i][j])
            handPointTotals[i] = handPointTotal
            print("Player " + str(i + 1) + "'s hand value: " + str(handPointTotal))
        if isBust(hands[dealerIndex]):
            for i in range(playerCount):
                if i in playersOut:
                    continue
                if not isBust(hands[i]):
                    print("Player " + str(i + 1) + " won!")
                    playerAccounts[i] += playerBets[i]
                else:
                    print("Player " + str(i + 1) + " lost.")
                    playerAccounts[i] -= playerBets[i]
        else:
            for i in range(playerCount):
                if i in playersOut:
                    continue
                if not isBust(hands[i]):
                    if handPointTotals[i] == handPointTotals[dealerIndex]:
                        print("Player " + str(i + 1) + " and the dealer tied.")
                    elif handPointTotals[i] < handPointTotals[dealerIndex]:
                        print("Player " + str(i + 1) + " lost.")
                        playerAccounts[i] -= playerBets[i]
                    elif handPointTotals[i] > handPointTotals[dealerIndex]:
                        print("Player " + str(i + 1) + " won!")
                        playerAccounts[i] += playerBets[i]
                else:
                    print("Player " + str(i + 1) + " busted. They lost. :(")
                    playerAccounts[i] -= playerBets[i]

        print("Balances")
        for i in range(playerCount):
            if i in playersOut:
                continue
            if playerAccounts[i] <= 0:
                playersOut.append(i)
                print("Player " + str(i + 1) + " is out of money")
            else:
                print("Player " + str(i + 1) + "'s balance: " + str(playerAccounts[i]) + "$")

        print("End of this round, folks.")
        sleep(2)
        print("Another round? (Y or N)")
        anotherRoundInput = input()
        anotherRound = True if anotherRoundInput == 'Y' or anotherRoundInput == 'y' else False
        if anotherRound:
            print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")

    print("\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n")

    if playerCount > 1:
        print("Thank you for playing!")
    else:
        print("Thank you all for playing!")

    playerAccountsSorted = []
    for i in range(playerCount):
        playerAccountsSorted.append(playerAccounts[i])
    sort(playerAccountsSorted)

    for i in playerAccountsSorted:
        print("Player " + str(playerAccounts.index(i) + 1) + ": " + str(i) + "$")
        playerAccounts[i] = -1


main()

