# Matthew Hill
# CS 1400 002
# Assignment 15 task 1
# Sorts a list of cards using bubble sort and insertion sort

from deck import Deck


def handString(list1):
    message = ""
    message = message + "["
    for i in range(len(list1)):
        message += str(list1[i].getNickName())
        if i != len(list1) - 1:
            message += ", "
        else:
            message += "]"
    return message


def drawHand(deck):
    deck.shuffle()
    hand = []
    for i in range(20):
        hand.append(deck.draw())

    return hand


def bubbleSort(list1):
    counter = 1
    print(str(counter) + ": " + handString(list1))
    counter += 1
    didSwap = True

    while didSwap:
        didSwap = False
        for i in range(len(list1) - 1):
            if list1[i].getCardValue() > list1[i + 1].getCardValue():
                list1[i], list1[i + 1] = list1[i + 1], list1[i]
                print(str(counter) + ": " + handString(list1))
                counter += 1
                didSwap = True


def insertionSort(list1):
    counter = 1
    print(str(counter) + ": " + handString(list1))
    counter += 1
    for i in range(1, len(list1)):
        currentElement = list1[i]
        j = i - 1
        while j >= 0 and list1[j].getCardValue() > currentElement.getCardValue():
            list1[j + 1] = list1[j]
            j -= 1
        list1[j + 1] = currentElement
        print(str(counter) + ": " + handString(list1))
        counter += 1


def main():
    deck = Deck()
    done = False
    while not done:
        hand = drawHand(deck)
        valid = False
        while not valid:
            print("Choose a sorting method: ")
            print("1) Bubble Sort")
            print("2) Insertion Sort")
            choice = input("Choose: ")
            if choice.isdigit() and eval(choice) != 1 or eval(choice) != 2:
                choice = eval(choice)
                valid = True
            else:
                print("Invalid input. Try again.")
                valid = False
        valid = False
        if choice == 1:
            bubbleSort(hand)
        else:
            insertionSort(hand)

        done = False if input("Want to go again with a different hand (Y or N)?") == 'Y' or 'y' else True








main()