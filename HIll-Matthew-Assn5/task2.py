# Matthew Hill
# CS 1400 002
# Assignment 5 Task 2
# Draws a target based on user input.

import turtle

print("You will tell this program how to make a target.\n")
radius = eval(input("Please enter a radius for the bullseye: "))

radius += 75

turtle.showturtle()
turtle.penup()
turtle.setheading(90)

# black circle
turtle.goto(radius, 0)
turtle.color("black")
turtle.fillcolor("black")
turtle.begin_fill()
turtle.pendown()
turtle.circle(radius)
turtle.end_fill()
turtle.penup()

# decrement
radius -= 25

# blue circle
turtle.goto(radius, 0)
turtle.color("blue")
turtle.fillcolor("blue")
turtle.begin_fill()
turtle.pendown()
turtle.circle(radius)
turtle.end_fill()
turtle.penup()

# decrement
radius -= 25

# red circle
turtle.goto(radius, 0)
turtle.color("red")
turtle.fillcolor("red")
turtle.begin_fill()
turtle.pendown()
turtle.circle(radius)
turtle.end_fill()
turtle.penup()

# decrement
radius -= 25

# bullseye
turtle.goto(radius, 0)
turtle.color("yellow")
turtle.fillcolor("yellow")
turtle.begin_fill()
turtle.pendown()
turtle.circle(radius)
turtle.end_fill()
turtle.penup()

turtle.done()