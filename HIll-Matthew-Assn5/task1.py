# Matthew Hill
# CS 1400 002
# Assignment 5, Task 1
# Calculates future investment value


originalInvestment = eval(input("Enter the investment amount: "))
annualRate = eval(input("Enter the annual interest rate, e.g. 6.75: "))
numYears = eval(input("Enter the number of years: "))
monthlyRate = annualRate / 1200
numMonths = numYears * 12
futureInvestmentValue = originalInvestment * (1 + monthlyRate) ** numMonths
print("The accumulate value is $" + str(int(futureInvestmentValue * 100) / 100) + ".")
