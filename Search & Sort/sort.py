def selectionSort(inputList):
    for i in range(len(inputList) - 1):
        curMinNumber = i

        for j in range(i + 1, len(inputList)):
            if inputList[curMinNumber] > inputList[j]:
                curMinNumber = j

        if curMinNumber != i:
            inputList[i], inputList[curMinNumber] = inputList[curMinNumber], inputList[i]


def bubbleSort(sortList):
    didSwap = True

    while didSwap:
        didSwap = False
        for i in range(len(sortList) - 1):
            if sortList[i] > sortList[i + 1]:
                sortList[i], sortList[i + 1] = sortList[i + 1], sortList[i]
                didSwap = True

def insertionSort(sortList):
    for i in range(1, len(sortList)):
        currentElement = sortList[i]
        j = i - 1
        while j >= 0 and sortList[j] > currentElement:
            sortList[j + 1] = sortList[j]
            j -= 1

        sortList[j + 1] = currentElement


def main():
    list = [5, 4, 7, 8, 1, 9, 3, 4, 5, 3, 4, 7, 8, 8, 2, 0]
    print(list)
    insertionSort(list)
    print(list)


main()