# Matthew Hill
# CS 1400 002
# Assignment 14 Task 3
# Main file for this task. Uses card class, deck class, and gubbinsutil module

from classes.deck import Deck
import modules.gubbinsutil as gubbins


def drawHand(deck):
    deck.shuffle()
    hand = []
    for i in range(30):
        hand.append(deck.draw())
    return hand


def selectionSortValue(hand):
    for i in range(len(hand) - 1):
        curMinIndex = i

        for j in range(i + 1, len(hand)):
            if hand[curMinIndex].getValue() > hand[j].getValue():
                curMinIndex = j

        if curMinIndex != i:
            hand[i], hand[curMinIndex] = hand[curMinIndex], hand[i]


def selectionSortID(hand):
    for i in range(len(hand) - 1):
        curMinIndex = i

        for j in range(i + 1, len(hand)):
            if hand[curMinIndex].getID() > hand[j].getID():
                curMinIndex = j

        if curMinIndex != i:
            hand[i], hand[curMinIndex] = hand[curMinIndex], hand[i]


def binarySearch(hand, key):
    selectionSortID(hand)
    low = 0
    high = len(hand) - 1
    while high >= low:
        mid = (high + low) // 2
        if key == hand[mid].getID():
            return True
        elif key < hand[mid].getID():
            high = mid - 1
        else:
            low = mid + 1

    return False


def main():
    deck = Deck()
    done = False
    hand = drawHand(deck)
    print("Welcome!\n")
    print("NEW HAND:")
    for i in hand:
        print(i)

    while not done:
        print()
        print("What would you like to do?")
        print("1) Sort hand by value")
        print("2) Sort hand by id")
        print("3) Find a card in your hand (automatically sorts by id)")
        print("4) Draw new hand")
        print("5) Exit the program")

        choice = eval(input("Choose: "))
        if choice == 1:
            selectionSortValue(hand)
            print()
            for i in hand:
                print(i)

        elif choice == 2:
            selectionSortID(hand)
            print()
            for i in hand:
                print(i)

        elif choice == 3:

            valid = False
            while not valid:
                print()
                searchValue = eval(input("What is the card's value? "))
                if searchValue >= 1 and searchValue <= 20:
                    valid = True
                else:
                    print("That value can't exist... Try again.")
                    valid = False

            valid = False
            while not valid:
                print()
                print("Mano?")
                print("1) Rock")
                print("2) Paper")
                print("3) Scissors")
                searchManoIndex = eval(input("Choose: ")) - 1
                if searchManoIndex >= 0 and searchManoIndex <=2:
                    searchMano = gubbins.mano[searchManoIndex]
                    valid = True
                else:
                    print("Yeah, no. Try again.")
                    valid = False

            valid = False
            while not valid:
                print()
                print("Coin?")
                print("1) Heads")
                print("2) Tails")
                searchCoinIndex = eval(input("Choose: ")) - 1
                if searchCoinIndex == 0 or searchCoinIndex == 1:
                    searchCoin = gubbins.coin[searchCoinIndex]
                    valid = True
                else:
                    print("Trying to create a new kind of coin, are we? Try again.")
                    valid = False

            searchID = gubbins.convertCardToValue(searchValue, searchMano, searchCoin)

            found = binarySearch(hand, searchID)
            if found:
                print()
                print("The card is in your hand!")
                print()
            else:
                print()
                print("The card is not in your hand :(")
                print()
        elif choice == 4:
            print()
            print("NEW HAND:")
            for i in hand:
                print(i)
            deck.shuffle()
            hand = drawHand(deck)

        elif choice == 5:
            done = True

        else:
            print("Yeah, that doesn't fly.")

    print("Bye!")


main()


