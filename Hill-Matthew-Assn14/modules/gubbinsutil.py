# Includes mano and coin maps and convertCardToValue functions for assn14-task3.py


mano = ["Rock", "Paper", "Scissors"]
coin = ["Heads", "Tails"]
maxCardValue = 20


def convertCardToValue(cardValue, cardMano, cardCoin):
    return 2 * ((cardValue - 1) + (maxCardValue * mano.index(cardMano))) + coin.index(cardCoin)

