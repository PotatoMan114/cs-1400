# Matthew Hill
# CS 1400 002
# Assignment 14 Task 2
# Take an unlimited number of numbers from user, display info about those numbers.






def main():
    numbers = []
    inputAgain = True
    while inputAgain:
        rawInput = input("Enter a number (leave blank to stop): ")
        if rawInput.isdigit():
            numbers.append(eval(rawInput))
        elif rawInput == "":
            inputAgain = False
        else:
            print("\nPlease enter a number.")

    print("There are " + str(len(numbers)) + " values")
    print("The smallest value is " + str(min(numbers)))
    print("The largest value is " + str(max(numbers)))
    sum = 0
    for i in numbers:
        sum += i
    print("The sum is " + str(sum))
    average = sum / len(numbers)
    print("The average is " + str(average))




main()
