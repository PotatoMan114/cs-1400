# Matthew Hill
# CS 1400 002
# Assignment 14 Task 3
# Deck class

from classes.card import Card
import random


class Deck:
    def __init__(self):
        self.shuffle()

    def draw(self):
        return self.__cards.pop()

    def shuffle(self):
        self.__cards = [None] * 120
        for i in range(0, 120):
            self.__cards[i] = Card(i)
        random.shuffle(self.__cards)

