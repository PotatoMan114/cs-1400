# Matthew Hill
# CS 1400 002
# Assignment 14 Task 3
# Card class

import modules.gubbinsutil as gubbins


class Card:
    def __init__(self, id):
        self.__id = id
        coinIndex = id % 2
        self.__coin = gubbins.coin[coinIndex]
        manoIndex = (id // 40) % 3
        self.__value = ((self.__id - coinIndex) // 2) - (20 * manoIndex) + 1
        self.__mano = gubbins.mano[manoIndex]

    def getID(self):
        return self.__id

    def getValue(self):
        return self.__value

    def getCoin(self):
        return self.__coin

    def getMano(self):
        return self.__mano

    def __str__(self):
        return str(self.__value) + " of " + self.__mano + " of " + self.__coin


