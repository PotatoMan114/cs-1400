# Matthew Hill
# CS 1400 002
# Assignment 13 Task 1
# main() outputs the results of +, -, <, >, ==, len(), and str() on two Polygon objects

from classes.polygon import Polygon


def main():
    polygon1 = Polygon(eval(input("Enter the number of sides of the first polygon: ")))
    polygon2 = Polygon(eval(input("Enter the number of sides for the second polygon: ")))

    print("Polygon 1 + Polygon 2: " + str(polygon1 + polygon2))
    print("Polygon 1 - Polygon 2: " + str(polygon1 - polygon2))
    print("Polygon 1 < Polygon 2: " + str(polygon1 < polygon2))
    print("Polygon 1 > Polygon 2: " + str(polygon1 > polygon2))
    print("Polygon 1 == Polygon 2: " + str(polygon1 == polygon2))
    print("length of polygon 1: " + str(len(polygon1)))
    print("length of polygon 2: " + str(len(polygon2)))
    print("string of polygon 1: " + str(polygon1))
    print("string of polygon 2: " + str(polygon2))



main()