# Matthew Hill
# CS 1400 002
# Assignment 13 Task 3
# List of Rectangle and Circle objects manipulated by user input and drawn using turtle.

import turtle
from classes.rectangle import Rectangle
from classes.circle import Circle


def invalidInput():
    print("Invalid input. Try again.")


def main():
    shapes = []
    colorList = ["red", "yellow", "blue", "green"]
    playAgain = True
    print("WELCOME!\nWhat would you like to do today?")
    while playAgain:
        valid = False
        print("Main Menu:")
        print("1) Enter a circle")
        print("2) Enter a rectangle")
        print("3) Remove a shape")
        print("4) Draw all shapes")
        print("5)Exit")
        while not valid:
            option = input("Enter an option: ")
            if not option.isdigit():
                invalidInput()
            elif eval(option) < 1 or eval(option) > 5:
                invalidInput()
            else:
                option = eval(option)
                valid = True
        valid = False
        if option == 1:
            centerX, centerY = eval(input("Enter the center point (x, y): "))
            while not valid:
                radius = input("Enter a radius: ")
                if not radius.isdigit():
                    invalidInput()
                elif eval(radius) <= 0:
                    invalidInput()
                else:
                    radius = eval(radius)
                    valid = True
            valid = False
            while not valid:
                print("Enter a color:\n1) Red\n2) Yellow\n3) Blue\n4) Green")
                colorInput = input("Pick one from above: ")
                if not colorInput.isdigit():
                    invalidInput()
                elif eval(colorInput) < 1 or eval(colorInput) > 4:
                    invalidInput()
                else:
                    colorInput = eval(colorInput)
                    color = colorList[colorInput - 1]
                    valid = True
            valid = False
            shapes.append(Circle(centerX, centerY, radius, color))
            print("Circle created.")
        elif option == 2:
            posX, posY = eval(input("Enter the point of the bottom left corner (x, y): "))
            while not valid:
                width = input("Enter a width: ")
                if not width.isdigit():
                    invalidInput()
                elif eval(width) <= 0:
                    invalidInput()
                else:
                    width = eval(width)
                    valid = True
            valid = False
            while not valid:
                height = input("Enter a height: ")
                if not height.isdigit():
                    invalidInput()
                elif eval(height) <= 0:
                    invalidInput()
                else:
                    height = eval(height)
                    valid = True
            valid = False
            while not valid:
                print("Enter a color:\n1) Red\n2) Yellow\n3) Blue\n4) Green")
                colorInput = input("Pick one from above: ")
                if not colorInput.isdigit():
                    invalidInput()
                elif eval(colorInput) < 1 or eval(colorInput) > 4:
                    invalidInput()
                else:
                    colorInput = eval(colorInput)
                    color = colorList[colorInput - 1]
                    valid = True
            valid = False
            shapes.append(Rectangle(posX, posY, width, height, color))
            print("Rectangle created.")

        elif option == 3:
            for i in range(len(shapes)):
                print("Pos: " + str(i) + "; " + str(shapes[i]))
            while not valid:
                choice = input("Enter the position of the shape you wish to delete: ")
                if not choice.isdigit():
                    invalidInput()
                elif eval(choice) < 0 or eval(choice) >= len(shapes):
                    invalidInput()
                else:
                    choice = eval(choice)
                    valid = True

            shapes.pop(choice)
        elif option == 4:
            if len(shapes) == 0:
                    print("There are no shapes to draw.\nClearing the screen.")
                    turtle.clear()
            else:
                turtle.clear()
                for shape in shapes:
                    shape.draw()

        elif option == 5:
            playAgain = False

    print("Have a nice day.")
    turtle.done()


main()