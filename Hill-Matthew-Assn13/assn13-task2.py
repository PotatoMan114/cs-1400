# Matthew Hill
# CS 1400 002
# Assignment 13 Task 2
# Outputs the count of numbers in a randomly generated list of 1,000 numbers 0 - 9
import random
from time import time


def main():
    random.seed(time())
    numbers = [random.randint(0, 9) for i in range(0, 1000)]
    count = [0 for i in range(0, 10)]
    for i in range(len(numbers)):
        count[numbers[i]] += 1
    print("Number: Count")
    for i in range(0, 10):
        print(str(i) + ": " + str(count[i]))


main()
