# Matthew Hill
# CS 1400 002
# Assignment 13 Task 3
# Circle class for assn13-task3.py

import turtle


class Circle:
    def __init__(self, centerX, centerY, radius, color):
        self.__centerX, self.__centerY = centerX, centerY
        self.__radius = radius
        self.__color = color

    def draw(self):
        turtle.penup()
        turtle.pencolor(self.__color)
        turtle.goto(self.__centerX, self.__centerY)
        turtle.setheading(270)
        turtle.forward(self.__radius)
        turtle.setheading(0)
        turtle.pendown()
        turtle.circle(self.__radius)
        turtle.penup()

    def __str__(self):
        return "Circle: (" + str(self.__centerX) + ", " + str(self.__centerY) + "); Radius: " + str(self.__radius) + "; " + str(self.__color)
