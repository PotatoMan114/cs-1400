# Matthew Hill
# CS 1400 002
# Assignment 13 Task 1
# Polygon class for assn13-task1.py


class Polygon:
    def __init__(self, sides):
        self.__sides = sides

    def getSides(self):
        return self.__sides

    # overloaded operators
    def __add__(self, other):
        return self.__sides + other.getSides()

    def __sub__(self, other):
        return self.__sides - other.getSides()

    def __lt__(self, other):
        return self.__sides < other.getSides()

    def __gt__(self, other):
        return self.__sides > other.getSides()

    def __eq__(self, other):
        return self.__sides == other.getSides()

    def __len__(self):
        return self.__sides

    def __str__(self):
        return "Polygon with " + str(self.__sides) + " sides"

