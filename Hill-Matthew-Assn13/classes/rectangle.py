# Matthew Hill
# CS 1400 002
# Assignment 13 Task 3
# Rectangle class for assn13-task3.py

import turtle


class Rectangle:
    def __init__(self, positionX, positionY, width, height, color):
        self.__positionX = positionX
        self.__positionY = positionY
        self.__width = width
        self.__height = height
        self.__color = color
        self.__colorList = ["red", "yellow", "blue", "green"]

    def draw(self):
        turtle.penup()
        turtle.color(self.__color)
        turtle.goto(self.__positionX, self.__positionY)
        turtle.setheading(0)
        turtle.pendown()
        for i in range(2):
            turtle.forward(self.__width)
            turtle.left(90)
            turtle.forward(self.__height)
            turtle.left(90)
        turtle.penup()

    def __str__(self):
        return "Rectangle: (" + str(self.__positionX) + ", " + str(self.__positionY) + "); " + "Width: " + str(self.__width) + "; Height: " + str(self.__height) + "; " + self.__color
