# Matthew Hill
# CS 1400 002
# Assignment 10
# This is the file that will be run.

import pattern


def main():
    # setup pattern
    pattern.setup()

    # Play again loop
    playAgain = True

    while playAgain:
        # Presnet a menu to the user
        # Let them select 'Super' mode or 'Single' mode
        print("Choose a mode")
        print("1) Rectangle Pattern")
        print("2) Circle Pattern")
        print("3) Super Pattern")
        mode = eval(input("Which mode do you want to play? 1, 2, or 3: "))
        # If they choose 'Rectangle Pattern'
        if mode == 1:
            # GET USER INPUT
            centerX, centerY = eval(input("Enter the center point (x, y): "))
            offset = eval(input("Enter the offset: "))
            width = eval(input("Enter the width of each rectangle: "))
            height = eval(input("Enter the height of each rectangle: "))
            count = eval(input("Enter the number of rectangles: "))
            rotation = eval(input("Enter the rotation: "))

            # Draw the rectangle pattern
            pattern.drawRectanglePattern(centerX, centerY, offset, width, height, count, rotation)

        # If they choose 'Circle Patterns'
        elif mode == 2:
            # GET USER INPUT
            centerX, centerY = eval(input("Enter the center point (x, y): "))
            offset = eval(input("Enter the offset: "))
            radius = eval(input("Enter the radius of each circle: "))
            count = eval(input("Enter the number of circles: "))

            # Draw the circle pattern
            pattern.drawCirclePattern(centerX, centerY, offset, radius, count)

        # If they choose 'Super Patterns'
        elif mode == 3:
            # GET USER INPUT
            num = input("How many patterns would you like to draw?: ")

            if num == "":
                pattern.drawSuperPattern()
            else:
                pattern.drawSuperPattern(eval(num))

        # Play again?
        print("Do you want to play again?")
        print("1) Yes, and keep drawings")
        print("2) Yes, and clear drawings")
        print("3) No, I am all done")
        response = eval(input("Choose 1, 2, or 3: "))

        if response == 1:
            playAgain = True

        elif response == 2:
            playAgain = True
            pattern.reset()
            pattern.setup()

        elif response == 3:
            playAgain = False

    # print a message saying thank you for playing
    print("Thanks for playing!")
    pattern.done()


main()
