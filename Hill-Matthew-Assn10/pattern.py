# Matthew Hill
# CS 1400 002
# Assignment 10
# The module pattern for assn10.py

import turtle
import random
import time

random.seed(time.time())


def reset():
    # Erase all of the patterns and start over
    turtle.penup()
    turtle.reset()
    turtle.hideturtle()


def setup():
    # Configure turtle to draw quickly
    turtle.screensize(1000, 800)
    turtle.speed(500)
    # turtle.speed(0)
    turtle.tracer(0, 0)


def drawRectanglePattern(centerX, centerY, offset, width, height, count, rotation):
    # center: the x, y center point of the circular pattern that is drawn
    # offset: This is the distance from the center position to the starting corner of each rectangle.
        # It can be a positive or negative number
    # height: The height of a rectangle
    # width: The width of a rectangle
    # count: The number of rectangles to draw within the 360 degree pattern.
    # rotation: The number of degrees each rectangle is rotated in relation to the line
        # from the center to the starting corner of the rectangle

    # Each rectangle is a random color
    for i in range(1, count + 1):
        turtle.penup()
        turtle.goto(centerX, centerY)
        angle = i * (360 / count)
        turtle.setheading(angle)
        turtle.forward(offset)
        turtle.right(rotation)
        drawRectangle(height, width)
        turtle.update()


def drawRectangle(height, width):
    # draw one rectangle
    setRandomColor()
    turtle.pendown()
    for i in range(0, 2):
        turtle.forward(height)
        turtle.left(90)
        turtle.forward(width)
        turtle.left(90)

    turtle.penup()


def drawCirclePattern(centerX, centerY, offset, radius, count):

    # center - This is the x, y center point of the circular pattern that is drawn
    # offset - This is the distance from the center position to starting drawing point of each circle.
        # It can be a positive or negative number. Note that the center point of each circle should be
        # 'radius + offset' distance from the Center Position of the pattern.
    # radius - The radius of the circle
    # count - The number of circles to draw within the 360 degree pattern.

    for i in range(0, count):
        turtle.penup()
        turtle.goto(centerX, centerY)
        angle = i * (360 / count)
        turtle.setheading(angle)
        turtle.forward(offset)
        turtle.right(90)
        setRandomColor()
        turtle.pendown()
        turtle.circle(radius)
        turtle.penup()
        turtle.update()


def drawSuperPattern(num=random.randint(1, 20)):
    # Randomly draw Rectangle and Circle patterns. Each pattern should be based on random values.
    # Values should be small enough for the pattern to be on the screen
    for i in range(0, num):
        rand = random.randint(0, 1)
        if rand == 0:
            drawRectanglePattern(random.randint(-400, 400), random.randint(-200, 200), random.randint(-100, 100),
                                 random.randint(5, 200), random.randint(5, 200), random.randint(2, 360),
                                 random.randint(-360, 360))
        if rand == 1:
            drawCirclePattern(random.randint(-400, 400), random.randint(-300, 300), random.randint(-100, 100),
                              random.randint(5, 100), random.randint(2, 360))


def setRandomColor():
    # set turtle to draw in a random color
    rand = random.randint(1, 7)

    if rand == 1:
        turtle.pencolor("red")

    elif rand == 2:
        turtle.pencolor("orange")

    elif rand == 3:
        turtle.pencolor("yellow")

    elif rand == 4:
        turtle.pencolor("green")

    elif rand == 5:
        turtle.pencolor("blue")

    elif rand == 6:
        turtle.pencolor("purple")

    elif rand == 7:
        turtle.pencolor("pink")

def done():
    # Keeps the turtle window open
    turtle.done()
