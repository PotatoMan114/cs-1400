System Requirements:
    user inputs x,y position for the bottom left corner of the chessboard
    user inputs the width and height of the board
    system uses turtle to draw one chessboard based on user's input.

System Analysis:
    individual rectangle height = total height / 8
    individual rectangle width = total height / 8

    right side of the board (x-value) = startX + width
    top of the board (y-value) = startY + height

System Design:
    Object-Oriented Program:
        Chessboard class

        Chessboard
        _____________________________________________________
        startX: int
        startY: int
        width: int
        height: int
        _____________________________________________________
        Chessboard(startX: int, startY: int, width=250: int, height=250: int)

        draw(): None
        drawAllRectangles(): None
        drawRectangle(width, height): None

    Use starter code


Testing:
    Test 1:
        0,0; width = ""; height = "";
        output is correct.

    Test 2:
        -200, 200; width = 100; height = 50;
        output is correct.

    Test 2:
        -400, -400; width = 240; height = 800
        output is correct.



