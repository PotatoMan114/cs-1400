# Matthew Hill
# CS 1400 002
# Assignment 12 Task 2
# Program tests if an inputted password is valid based on 5 criteria using a Password class.

from password import Password


def main():
    password = Password()
    playAgain = True
    print("This program will test the validity of a password based on 5 criteria:")
    print("1) Must have at least 8 characters")
    print("2) Must consist of only letters and digits")
    print("3) Must contain at least 2 digits")
    print("4) Cannot contain \"password\"")
    print("5) Cannot end with \"123\"")
    while(playAgain):
        password.setPassword(input("Enter a password to test: "))
        if password.isValid():
            print("The password is valid.", end="\n\n")
        else:
            print("The password is not valid.")
            print(password.getErrorMessage())
        playAgainRaw = input("Would you like to test another password? (Y or N)")
        playAgain = True if (playAgainRaw == 'Y' or playAgainRaw == 'y') else False

    print("Thanks for stopping by!")


main()
