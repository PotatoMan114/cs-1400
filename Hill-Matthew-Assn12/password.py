# Matthew Hill
# CS 1400 002
# Assignment 12 Task 2
# Password class for assn12-task2.py

class Password:
    def __init__(self, password=""):
        self.__password = password
        self.__message = ""

    def setPassword(self, password):
        self.__password = password

    def isValid(self):
        valid = True
        self.__message = ""
        # All checks are run in the if statements, and all error messages are added within the methods.
        if not self.__hasEightCharacters():
            valid = False
        if not self.__onlyAlphaNumeric():
            valid = False
        if not self.__hasTwoDigits():
            valid = False
        if not self.__doesNotContainPassword():
            valid = False
        if not self.__doesNotEndWith123():
            valid = False

        return valid

    def __hasEightCharacters(self):
        if len(self.__password) < 8:
            self.__message = self.__message + "Password must be more than 8 letters.\n"
            return False
        else:
            return True

    def __onlyAlphaNumeric(self):
        if not self.__password.isalnum():
            self.__message = self.__message + "Password can only contain alpha-numeric characters\n"
            return False
        else:
            return True

    def __hasTwoDigits(self):
        digitCount = 0
        for i in range(len(self.__password)):
            if self.__password[i].isdigit():
                digitCount += 1

        if digitCount < 2:
            self.__message = self.__message + "Password must contain at least 2 digits.\n"
            return False
        else:
            return True

    def __doesNotContainPassword(self):
        if "password" in self.__password.lower():
            self.__message = self.__message + "Password cannot contain \"password\"\n"
            return False
        else:
            return True

    def __doesNotEndWith123(self):
        if self.__password.endswith("123"):
            self.__message = self.__message + "Password cannot end with \"123\"\n"
            return False
        else:
            return True

    def getErrorMessage(self):
        return self.__message
