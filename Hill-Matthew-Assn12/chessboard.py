# Matthew Hill
# CS 1400 002
# Assignment 12 Task 1
# Chessboard class for assn12-task1.py

import turtle


class Chessboard:
    def __init__(self, startX, startY, width=250, height=250):
        self.__startX, self.__startY = startX, startY
        self.__width = width
        self.__height = height

    def draw(self):
        # turtle goes to x, y
        turtle.penup()
        turtle.speed(0)
        turtle.goto(self.__startX, self.__startY)

        # draw the border
        turtle.pendown()
        for i in range(0, 2):
            turtle.forward(self.__width)
            turtle.left(90)
            turtle.forward(self.__height)
            turtle.left(90)
        turtle.penup()
        self.__drawAllRectangles()

    def __drawAllRectangles(self):
        __oneWidth = self.__width / 8  # the width of one rectangle
        __oneHeight = self.__height / 8  # the height of one rectangle

        for x in range(self.__startX, int(self.__startX + self.__width - __oneWidth + 1), int(__oneWidth * 2)):
            for y in range(self.__startY, int(self.__startY + self.__height - __oneHeight + 1), int(__oneHeight * 2)):
                turtle.penup()
                turtle.setheading(0)
                turtle.goto(x, y)
                self.__drawRectangle(__oneWidth, __oneHeight)

        for x in range(int(self.__startX + __oneWidth), self.__startX + self.__width + 1, int(__oneWidth * 2)):
            for y in range(int(self.__startY + __oneHeight), self.__startY + self.__height + 1, int(__oneHeight * 2)):
                turtle.penup()
                turtle.setheading(0)
                turtle.goto(x, y)
                self.__drawRectangle(__oneWidth, __oneHeight)

    def __drawRectangle(self, width, height):
        turtle.pendown()
        turtle.fillcolor("black")
        turtle.begin_fill()
        for i in range(0, 2):
            turtle.forward(width)
            turtle.left(90)
            turtle.forward(height)
            turtle.left(90)
        turtle.penup()
        turtle.end_fill()

