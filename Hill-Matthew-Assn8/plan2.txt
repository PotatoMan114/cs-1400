System Requirements:
    User inputs if they wish to switch or stay.
    System outputs the number of times and percentage of times they won out of 100,000 iterations.

System Analysis:
    use random module for randrange
    100,000 iterations (for loop)

    percentage of times won = times won / 100,000

System Design:
    import time
    import random
    seed random using time()

    initialize playAgain as True
    while the user wants to play again (playAgain variable):
        initialize userDoor as 0 (no door chosen)
            userDoor is the door that the player initially chose in that iteration.
        initialize hostDoor as 0 (no door chosen)
            hostDoor is the door that the host opens that is a goat.
        initialize winCount as 0
        prompt user for stay/switch
        for loop increasing i by one in range [1, 100000]
            carDoor = random number in range [1, 3]
            userDoor = random number in range [1, 3]
            hostDoor = random number in range [1, 3] and is not equal to userDoor and is not equal to carDoor

            if user chose to switch:
                if hostDoor is 1:
                    if userDoor is 2:
                        userDoor changes to 3
                    else:
                        userDoor changes to 2
                else if hostDoor is 2:
                    if userDoor is 1:
                        userDoor changes to 3
                    else:
                        userDoor changes to 1
                else:
                    if userDoor is 1:
                        userDoor changes to 2
                    else:
                        userDoor changes to 1

            if userDoor = carDoor:
                the user wins, add one to win count.

        After all 100,000 iterations:
            print win percentage and win count.

        Ask the user if they would like to play again.

    end the program.


Testing:
    If the user switches, they should win about 66.66% of the time.
    If the user stays, they should win about 33.33 % of the time.

