# Matthew Hill
# CS 1400 002
# Assignment 8 Task 2
# Monty Hall problem proof

import time
import random

random.seed(time.time())

playAgain = True

while playAgain:
    userDoor = 0
    hostDoor = 0
    winCount = 0
    switchInput = eval(input("Would you like to test (1) staying or (2) switching: "))
    switch = True if switchInput == 2 else False

    for i in range(1, 100001):
        carDoor = random.randint(1, 3)
        userDoor = random.randint(1, 3)
        hostDoor = random.randint(1, 3)
        while hostDoor == userDoor or hostDoor == carDoor:
            hostDoor = random.randint(1, 3)

        if switch:
            if hostDoor == 1:
                if userDoor == 2:
                    userDoor = 3
                else:
                    userDoor = 2
            elif hostDoor == 2:
                if userDoor == 1:
                    userDoor = 3
                else:
                    userDoor = 1
            else:
                if userDoor == 1:
                    userDoor = 2
                else:
                    userDoor = 1

        if userDoor == carDoor:
            winCount += 1

    percentageWin = winCount / 100000
    print("You won " + str(winCount) + " times, or " + str(percentageWin) + "%.", end="\n\n")
    playAgain = bool(eval(input("Do you want to play again (0: No, 1: Yes)")))

print("See you later.", end="\n\n")

































