# Matthew Hill
# CS 1400 002
# Assignment 8 Task 1
# Find the four perfect numbers before 10,000
"""
import time

startTime = time.time()
count = 0
perfectNumberCount = 0
for number in range(1, 10001):
    divisorSum = 0
    for divisor in range(1, number // 2 + 1):
        count += 1
        if number % divisor == 0 and divisor != number:
            divisorSum += divisor
        else:
            continue
        if divisorSum > number:
            break
    if divisorSum == number:
        print(str(number) + " is a perfect number")
        perfectNumberCount += 1
        if perfectNumberCount == 4:
            break


print("\n")
print("The program went through " + str(count) + " iterations")
endTime = round(time.time() - startTime, 3)

print("The program took " + str(endTime) + " seconds to complete.")

"""
iterations = 0
for i in range (1, 10001, 1):
    sumOfDevisors = 0
    for j in range (1, i+1):
        iterations += 1
        if i % j == 0 and i != j:
            sumOfDevisors += j
    if sumOfDevisors == i:
        print(i, "is a perfect number")
    else:
        continue
print("iterations =", iterations)